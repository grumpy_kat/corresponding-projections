import pandas as pd

import numpy as np
import pickle
from scipy import sparse
import svr

#Root Mean Squared Error
def rmse(predictions, targets):
    return np.sqrt(((predictions - targets) ** 2).mean())

kernel = "linear"

for draw in range(10):
    print("Draw {}".format(draw))
    draw_folder = "../data/fixed_datasets/minLines_260_undersampling_True_numberDraws_10/"+str(draw)

    protein_XY_dict = pickle.load(open(draw_folder+"/protein_XY_dict.p","rb"))
    compound_to_index = pickle.load(open(draw_folder+"/compound_to_index.p","rb"))
    protein_index_to_compound = pickle.load(open(draw_folder+"/protein_index_to_compound.p","rb"))
    compound_feature = pickle.load(open(draw_folder+"/compound_feature.p","rb"))
    target_compound_sets = pickle.load(open(draw_folder+"/target_compound_sets.p","rb"))
    order_per_protein = pickle.load(open(draw_folder+"/order_per_protein.p","rb"))
    max_entry = pickle.load(open(draw_folder+"/max_entry.p","rb"))



    #calculate gram matrix
    compound_matrix = []
    for i in range(len(compound_feature)):
        compound_matrix.append(compound_feature[i])
    compound_matrix = sparse.vstack(np.array(compound_matrix))

    import tanimotoKernel

    gram_matrix = None
    if kernel == "linear":
        gram_matrix = np.dot(compound_matrix,compound_matrix.transpose())
        gram_matrix = gram_matrix.todense()
        gram_matrix = gram_matrix + np.ones(gram_matrix.shape)
    elif kernel == "tanimoto":
        gram_matrix = tanimotoKernel.computeTanimotoMatrix(compound_matrix.todense())
        gram_matrix += np.ones(gram_matrix.shape)/15808


    import sklearn

    #store the best model per protein in dictionary
    protein_models = {}
    #create an SVR and optimize via gridsearch per protein
    for protein in protein_XY_dict:
        from sklearn.model_selection import KFold

        kf = KFold(n_splits=10)
        X = protein_XY_dict[protein]["X"]
        y = protein_XY_dict[protein]["y"]
        param_grid = {"epsilon": [0.1, 0.01, 0.001], "C": [2 ** i for i in range(-5, 6)]}
        from sklearn.model_selection import ParameterGrid
        param_tuples = ParameterGrid(param_grid)

        protein_gram = gram_matrix[order_per_protein[protein],:]
        protein_gram = protein_gram[:,order_per_protein[protein]]

        best_rmse = np.inf
        for parameter in param_tuples:
            p_rmse = []
            for train_index, test_index in kf.split(X):
                relevant_gram = protein_gram[train_index,:]
                relevant_gram = relevant_gram[:,train_index]
                Xids = np.array(order_per_protein[protein])
                Xids = Xids[train_index]
                svs, coefs = svr.svr_train(relevant_gram, Xids , y[train_index], parameter["C"], parameter["epsilon"])



                K_test_svs = gram_matrix[order_per_protein[protein],:]
                K_test_svs = K_test_svs[test_index,:]
                K_test_svs = K_test_svs[:,svs]


                support_vectors = np.array([compound_feature[v] for v in svs])
                comparable_coefs =  sum([coefs[i]*support_vectors[i] for i in range(len(support_vectors))]).todense()

                #print("Sklearn coefs are {}".format(sk_svr.coef_))
                #print("Our SVR coefs are {}".format(comparable_coefs))

                #print("Difference is {}".format(np.sum(np.abs(sk_svr.coef_-comparable_coefs))))

                #print("Average differeonce per coeff is {}".format(average_diff))
                #print("Average coeff value is {}".format(average_coef))
                #print("Procentual difference per entry is {}".format(average_diff/average_coef))


                y_pred = np.array(svr.predict(K_test_svs,np.array(coefs).T)).reshape((len(test_index)))
                p_rmse.append(rmse(y_pred, y[test_index]))

            average_rmse = np.average(p_rmse)
            if best_rmse > average_rmse:
                Xids = np.array(order_per_protein[protein])
                svs, coefs = svr.svr_train(protein_gram, Xids, y, parameter["C"], parameter["epsilon"])
                protein_models[protein] = (svs,coefs)
                best_rmse = average_rmse
                print("Best RMSE for protein {} is {}".format(protein,best_rmse))



    with open(draw_folder+"/compound_matrix_"+kernel+".p","wb") as f:
        pickle.dump(compound_matrix,f)

    with open(draw_folder+"/gram_matrix_"+kernel+".p","wb") as f:
        pickle.dump(gram_matrix, f)

    with open(draw_folder+"/protein_models_"+kernel+".p","wb") as f:
        pickle.dump(protein_models,f)


