import numpy as np
import pandas as pd
import svr
import pickle
from scipy.stats import kendalltau

draw = 0
kernel = "linear"

draw_folder = "../data/fixed_datasets/minLines_240_undersampling_True_numberDraws_10/"+str(draw)

protein_XY_dict = pickle.load(open(draw_folder+"/protein_XY_dict.p","rb"))
compound_to_index = pickle.load(open(draw_folder+"/compound_to_index.p","rb"))
protein_index_to_compound = pickle.load(open(draw_folder+"/protein_index_to_compound.p","rb"))
compound_feature = pickle.load(open(draw_folder+"/compound_feature.p","rb"))
target_compound_sets = pickle.load(open(draw_folder+"/target_compound_sets.p","rb"))
order_per_protein = pickle.load(open(draw_folder+"/order_per_protein.p","rb"))
max_entry = pickle.load(open(draw_folder+"/max_entry.p","rb"))
gram_matrix = pickle.load(open(draw_folder+"/gram_matrix_"+kernel+".p","rb"))
protein_models = pickle.load(open(draw_folder+"/protein_models_"+kernel+".p","rb"))
protein_sims = pickle.load(open(draw_folder+"/protein_sims.p","rb"))
minLines = pickle.load(open(draw_folder+"/minLines.p","rb"))
double_check = pickle.load(open(draw_folder+"/double_check.p","rb"))
maxProteins = pickle.load(open(draw_folder+"/maxProteins.p","rb"))

models = ["NLCP","SimplifiedAlgorithm", "SimplifiedAlgorithmWeighted", "Averaged", "Closest"]


#create a list of all protein names
proteins = list(protein_XY_dict.keys())

#Root Mean Squared Error
def rmse(predictions, targets):
    return np.sqrt(((predictions - targets) ** 2).mean())


#Create a dataframe to store results
index = pd.Series(proteins)
column_names = proteins + models + ["Best"]
result_df = pd.DataFrame(index=index, columns=column_names)

bests = []
res_dict = {}


#CALCULATE NLCP AND OTHER BASELINES
import random

rmse_file = open(draw_folder+"/rmse_"+str(minLines)+"_Ligands_"+str(maxProteins)+"_MaxProteins_Kernel_"+kernel+"_DoubleCheck_"+str(double_check)+"_reg_"+str(1),"w")
rmse_file.write("Protein;NLCP;SIMPLIFIED;TLK;TLK-3;AVERAGED;AVERAGED-3;CLOSEST;FARTHEST;AVERAGE-Y;BESTPROTEIN;SUPER50\n")

kt_file = open(draw_folder+"/kt_"+str(minLines)+"_Ligands_"+str(maxProteins)+"_MaxProteins_Kernel_"+kernel+"_DoubleCheck_"+str(double_check)+"_reg_"+str(1),"w")
kt_file.write("Protein;NLCP;SIMPLIFIED;TLK;TLK-3;AVERAGED;AVERAGED-3;CLOSEST;FARTHEST;AVERAGE-Y;BESTPROTEIN;SUPER50\n")

for orphan in proteins:

    #The indices of the orphan bindings
    orphan_indices = np.array(order_per_protein[orphan])
    #Within the indices of the orphan, the indices which we want to use
    indices = list(range(minLines))

    # all proteins except for the orphan protein
    other_proteins = np.array([p for p in proteins if p != orphan])
    print("Other proteins are: {}".format(other_proteins))

    print("We have {} orphan indices and are using {} for prediction".format(len(orphan_indices),len(indices)))

    # SUPER50
    # Set of indices for validation and testing in the case of the supervised-50% model
    validation_index_50 = int(len(indices) * 0.5)
    validation_indices_50 = indices[0:validation_index_50]
    print("Validation set size is {}".format(len(validation_indices_50)))
    test_indices_50 = indices[validation_index_50:]
    print("Test set size is {}".format(len(test_indices_50)))
    print("Prediction for Protein {}".format(orphan))

    # Y-Values of the orphan plus Y-Average benchmark
    y = protein_XY_dict[orphan]["y"]
    y_validation_50 = protein_XY_dict[orphan]["y"][validation_indices_50]
    y_test_50 = protein_XY_dict[orphan]["y"][test_indices_50]

    # SUPER5
    # Set of indices for validation and testing in the case of the supervised-5% model
    validation_index_5 = int(len(indices) * 0.05)
    validation_indices_5 = indices[0:validation_index_5]
    print("Validation set size is {}".format(len(validation_indices_5)))
    test_indices_5 = indices[validation_index_5:]
    print("Test set size is {}".format(len(test_indices_5)))
    print("Prediction for Protein {}".format(orphan))

    # Y-Values of the orphan plus Y-Average benchmark
    y = protein_XY_dict[orphan]["y"]
    y_validation_5 = protein_XY_dict[orphan]["y"][validation_indices_5]
    y_test_5 = protein_XY_dict[orphan]["y"][test_indices_5]

    # SUPER10
    # Set of indices for validation and testing in the case of the supervised-10% model
    validation_index_10 = int(len(indices) * 0.1)
    validation_indices_10 = indices[0:validation_index_10]
    print("Validation set size is {}".format(len(validation_indices_10)))
    test_indices_10 = indices[validation_index_10:]
    print("Test set size is {}".format(len(test_indices_10)))
    print("Prediction for Protein {}".format(orphan))

    # Y-Values of the orphan plus Y-Average benchmark
    y = protein_XY_dict[orphan]["y"]
    y_validation_10 = protein_XY_dict[orphan]["y"][validation_indices_10]
    y_test_10 = protein_XY_dict[orphan]["y"][test_indices_10]

    # SUPER30
    # Set of indices for validation and testing in the case of the supervised-30% model
    validation_index_30 = int(len(indices) * 0.3)
    validation_indices_30 = indices[0:validation_index_30]
    print("Validation set size is {}".format(len(validation_indices_30)))
    test_indices_30 = indices[validation_index_30:]
    print("Test set size is {}".format(len(test_indices_30)))
    print("Prediction for Protein {}".format(orphan))

    # Y-Values of the orphan plus Y-Average benchmark
    y = protein_XY_dict[orphan]["y"]
    y_validation_30 = protein_XY_dict[orphan]["y"][validation_indices_30]
    y_test_30 = protein_XY_dict[orphan]["y"][test_indices_30]

    # SUPER80
    # Set of indices for validation and testing in the case of the supervised-30% model
    validation_index_80 = int(len(indices) * 0.8)
    validation_indices_80 = indices[0:validation_index_80]
    print("Validation set size is {}".format(len(validation_indices_80)))
    test_indices_80 = indices[validation_index_80:]
    print("Test set size is {}".format(len(test_indices_80)))
    print("Prediction for Protein {}".format(orphan))

    # Y-Values of the orphan plus Y-Average benchmark
    y = protein_XY_dict[orphan]["y"]
    y_validation_80 = protein_XY_dict[orphan]["y"][validation_indices_80]
    y_test_80 = protein_XY_dict[orphan]["y"][test_indices_80]


    other_y_s = []
    for op in other_proteins:
        for y_value in protein_XY_dict[op]["y"]:
           other_y_s.append(y_value)
    y_other_average = np.average(other_y_s)
    y_other_std = np.std(other_y_s)

    print("Average y of orphan protein is: {}".format(np.average(y)))
    print("Average standard deviation of y of orphan protein is: {}".format(np.std(y)))
    print("Average y of other proteins is: {}".format(y_other_average))
    print("Average standard deviation of y of other proteins is: {}".format(y_other_std))
    average_y = np.array([y_other_average]*len(y))
    average_y_rmse = rmse(np.array(y),average_y)
    average_y_kt = kendalltau(np.array(y),average_y)[0]
    print("RMSE for average y is {}".format(average_y_rmse))
    print("KT for average y is {}".format(average_y_kt))


    # building PI-Matrix for protein
    pi_matrix = np.array([])
    pi_list = []
    for op in other_proteins:
        pi = np.zeros((gram_matrix.shape[0], 1))
        pi[protein_models[op][0], 0] = protein_models[op][1]
        pi_list.append(pi)
    pi_matrix = np.hstack(pi_list).reshape(pi.shape[0], len(pi_list))

    factors = []
    estimators = []

    # For all other proteins obtain their similarities to orphan protein
    # and their best models
    for op in other_proteins:
        factors.append(protein_sims[orphan][op])

    factors = np.array(factors)/sum(factors)
    print("Factors are {}".format(factors))


    closest_3_positions = factors.argsort()[-3:][::-1]
    closest_3_factors = factors[closest_3_positions]
    closest_3_proteins = np.array(other_proteins)[closest_3_positions]
    print("Closest 3 are at position: {}\n Closest 3 proteins are {} with factors {} ".format(closest_3_positions, closest_3_proteins, closest_3_factors))


    # building PI-Matrix for closest3 for protein (do we need to calculate a seperate gram-matrix?)
    closest_3_pi_matrix = np.array([])
    pi_list = []
    for op in closest_3_proteins:
        pi = np.zeros((gram_matrix.shape[0], 1))
        pi[protein_models[op][0], 0] = protein_models[op][1]
        pi_list.append(pi)
    closest_3_pi_matrix = np.hstack(pi_list).reshape(pi.shape[0], len(pi_list))


    #Closest Protein
    closest_protein_index = np.argmax(factors)
    closest_protein = other_proteins[closest_protein_index]
    print("The closest protein is {} with a factor of {}".format(closest_protein,factors[closest_protein_index]))

    p_closest = np.dot(gram_matrix, pi_matrix[:, closest_protein_index])
    p_closest = np.squeeze(np.asarray(p_closest))
    closest_est_predictions = p_closest[orphan_indices]
    closest_est_predictions = p_closest[indices]
    closest_est_predictions = closest_est_predictions.reshape(1,closest_est_predictions.shape[0])
    closest_rmse = rmse(np.array(y), np.array(closest_est_predictions))
    closest_kt = kendalltau(np.array(y),np.array(closest_est_predictions))[0]

    #Farthest Protein
    farthest_protein_index = np.argmin(factors)
    farthest_protein = other_proteins[farthest_protein_index]
    print("The farthest protein is {} with a factor of {}".format(farthest_protein,factors[farthest_protein_index]))

    p_farthest = np.dot(gram_matrix, pi_matrix[:, farthest_protein_index])
    p_farthest = np.squeeze(np.asarray(p_farthest))
    farthest_est_predictions = p_farthest[orphan_indices]
    farthest_est_predictions = p_farthest[indices]
    farthest_est_predictions = farthest_est_predictions.reshape(1,farthest_est_predictions.shape[0])
    farthest_rmse = rmse(np.array(y), np.array(farthest_est_predictions))
    farthest_kt = kendalltau(np.array(y), np.array(farthest_est_predictions))[0]

    #CALCULATE NLCP

    # find best nu
    regularization_term = 1

    #CALCULATING NLCP
    #Pi.T * K * Pi
    G = np.dot(pi_matrix.T,np.dot(gram_matrix,pi_matrix))
    N = np.identity(G.shape[1])

    # nu * G
    #left_term = regularization_term *  np.diag(np.diag(G)) # Gecko did this. This is a matrix with only the diagonal of G, the rest is zero
    left_term = regularization_term * G

    #G * N * G
    middle_term = np.dot(G,np.dot(N,G))

    #
    A = left_term + middle_term

    I = np.eye(A.shape[0])

    lmbd = min(np.linalg.eig(A)[0])

    print("Lambda is : {}".format(lmbd))

    #[nu * G + G*N*G]^-1
    left_term = np.linalg.inv(A+lmbd*I)

    #rho_0 = k_T * sqrt({G}_ii)
    rho_0 = np.array([protein_sims[orphan][other_proteins[i]] * np.sqrt(G[i,i]) for i in range(len(other_proteins))]).reshape(len(other_proteins),1)

    #beta_0 = [nu * G + G*N*G]^-1 * G +rho_0
    beta_0 = np.dot(left_term,np.dot(G,rho_0))
    beta_0 /= np.linalg.norm(beta_0,1)

    #h_0(x) = {K * Pi_Matrix * beta_0}x
    h_0 = np.dot(gram_matrix,np.dot(pi_matrix,beta_0))

    # Regularization Term nu
    #print("Regularization Term nu is set to {}".format(regularization_term))
    protein_models["NLCP"] = (pi_matrix,beta_0)


    nlcp_est_predictions = h_0[orphan_indices, :]
    nlcp_est_predictions = nlcp_est_predictions[indices, :]
    nlcp_est_predictions = nlcp_est_predictions.reshape(1, nlcp_est_predictions.shape[0])
    nlcp_rmse = rmse(np.array(y), np.array(nlcp_est_predictions))
    nlcp_kt = kendalltau(np.array(y), np.array(nlcp_est_predictions))[0]

    #SIMPLIFIED
    h_simplified = np.dot(gram_matrix,np.dot(pi_matrix,factors.reshape(factors.shape[0],1)))
    simplified_est_predictions = h_simplified[orphan_indices, :]
    simplified_est_predictions = simplified_est_predictions[indices, :]
    simplified_est_predictions = simplified_est_predictions.reshape(1, simplified_est_predictions.shape[0])
    simplified_rmse = rmse(np.array(y),np.array(simplified_est_predictions))
    simplified_kt = kendalltau(np.array(y),np.array(simplified_est_predictions))[0]

    #AVERAGED
    uniform_factors = np.ones((factors.shape[0],1))/len(factors)
    print("Uniform Factors: {}".format(uniform_factors))
    h_averaged = np.dot(gram_matrix,np.dot(pi_matrix,uniform_factors))
    averaged_est_predictions = h_averaged[orphan_indices, :]
    averaged_est_predictions = averaged_est_predictions[indices, :]
    averaged_est_predictions = averaged_est_predictions.reshape(1, averaged_est_predictions.shape[0])
    averaged_rmse = rmse(np.array(y),np.array(averaged_est_predictions))
    averaged_kt = kendalltau(np.array(y),np.array(averaged_est_predictions))[0]

    #AVERAGED-Closest-3
    uniform_closest_3_factors = np.ones((closest_3_factors.shape[0],1))/len(closest_3_factors)
    print("Uniform Closest-3 Factors: {}".format(uniform_closest_3_factors))
    h_averaged_3 = np.dot(gram_matrix,np.dot(closest_3_pi_matrix,uniform_closest_3_factors))
    averaged_3_est_predictions = h_averaged_3[orphan_indices, :]
    averaged_3_est_predictions = averaged_3_est_predictions[indices, :]
    averaged_3_est_predictions = averaged_3_est_predictions.reshape(1, averaged_3_est_predictions.shape[0])
    averaged_3_rmse = rmse(np.array(y),np.array(averaged_3_est_predictions))
    averaged_3_kt = kendalltau(np.array(y),np.array(averaged_3_est_predictions))[0]

    # Best-Protein
    best_protein = None
    best_protein_rmse = np.inf

    best_kt_protein = None
    best_protein_kt = -1

    for i in range(len(other_proteins)):
        p_other = np.dot(gram_matrix, pi_matrix[:,i])
        p_other = np.squeeze(np.asarray(p_other))
        op_est_predictions = p_other[orphan_indices]
        op_est_predictions = p_other[indices]
        op_est_predictions = op_est_predictions.reshape(1,op_est_predictions.shape[0])
        op_rmse = rmse(np.array(y), np.array(op_est_predictions))
        op_kt = kendalltau(np.array(y), np.array(op_est_predictions))[0]

        if op_rmse < best_protein_rmse:
            best_protein = other_proteins[i]
            best_protein_rmse = op_rmse

        if op_kt > best_protein_kt:
            best_kt_protein = other_proteins[i]
            best_protein_kt = op_kt

    param_grid = {"epsilon": [0.1, 0.01, 0.001], "C": [2 ** i for i in range(-5, 6)]}
    from sklearn.model_selection import ParameterGrid
    param_tuples = ParameterGrid(param_grid)
    from sklearn.model_selection import KFold
    kf = KFold(n_splits=3)

    def get_supervised_rmse_and_kt(validation_indices,test_indices,param_tuples):
        # Supervised on 50%
        from sklearn.model_selection import KFold

        kf = KFold(n_splits=3)
        X = protein_XY_dict[orphan]["X"][validation_indices]
        y = protein_XY_dict[orphan]["y"]
        y_val = y[validation_indices]


        protein_gram = gram_matrix[orphan_indices, :]
        protein_gram = protein_gram[:, orphan_indices]
        protein_val_gram = protein_gram[validation_indices, :]
        protein_val_gram = protein_val_gram[:, validation_indices]

        best_rmse = np.inf
        best_parameter = None

        best_kt = -1
        best_parameter_kt = None

        for parameter in param_tuples:
            p_rmse = []
            p_kt = []
            for train_index, test_index in kf.split(X):
                relevant_gram = protein_val_gram[train_index, :]
                relevant_gram = relevant_gram[:, train_index]
                Xids = np.array(orphan_indices)[validation_indices]
                Xids = Xids[train_index]
                svs, coefs = svr.svr_train(relevant_gram, Xids, y_val[train_index], parameter["C"],
                                           parameter["epsilon"])

                K_test_svs = gram_matrix[orphan_indices, :]
                K_test_svs = K_test_svs[validation_indices, :]
                K_test_svs = K_test_svs[test_index, :]
                K_test_svs = K_test_svs[:, svs]

                y_pred = np.array(svr.predict(K_test_svs, np.array(coefs).T)).reshape((len(test_index)))

                p_rmse.append(rmse(y_pred, y_val[test_index]))
                p_kt.append(kendalltau(y_pred, y_val[test_index])[0])

            average_rmse = np.average(p_rmse)
            if best_rmse > average_rmse:
                protein_models[orphan] = (svs, coefs)
                best_rmse = average_rmse
                best_parameter = parameter

            average_kt = np.average(p_kt)
            if average_kt > best_kt:
                best_kt = average_kt
                best_parameter_kt = parameter

        relevant_gram = protein_gram[validation_indices, :]
        relevant_gram = relevant_gram[:, validation_indices]
        Xids = np.array(orphan_indices)
        Xids = Xids[validation_indices]
        svs, coefs = svr.svr_train(relevant_gram, Xids, y[validation_indices], best_parameter["C"],
                                   best_parameter["epsilon"])

        K_test_svs = gram_matrix[orphan_indices, :]
        K_test_svs = K_test_svs[test_indices, :]
        K_test_svs = K_test_svs[:, svs]

        y_pred = np.array(svr.predict(K_test_svs, np.array(coefs).T)).reshape((len(test_indices)))

        super_rmse = rmse(y_pred, y[test_indices])

        svs, coefs = svr.svr_train(relevant_gram, Xids, y[validation_indices], best_parameter_kt["C"],
                                   best_parameter_kt["epsilon"])

        K_test_svs = gram_matrix[orphan_indices, :]
        K_test_svs = K_test_svs[test_indices, :]
        K_test_svs = K_test_svs[:, svs]

        y_pred = np.array(svr.predict(K_test_svs, np.array(coefs).T)).reshape((len(test_indices)))

        super_kt = kendalltau(y_pred, y[test_indices])[0]

        return (super_rmse,super_kt)


    super_05_rmse, super_05_kt = get_supervised_rmse_and_kt(validation_indices_5, test_indices_5,param_tuples)
    super_10_rmse, super_10_kt = get_supervised_rmse_and_kt(validation_indices_10, test_indices_10,param_tuples)
    super_30_rmse, super_30_kt = get_supervised_rmse_and_kt(validation_indices_30, test_indices_30,param_tuples)
    super_50_rmse, super_50_kt = get_supervised_rmse_and_kt(validation_indices_50, test_indices_50,param_tuples)
    super_80_rmse, super_80_kt = get_supervised_rmse_and_kt(validation_indices_80, test_indices_80,param_tuples)


    ### Target-Ligand Kernel
    # building K_X for TLK
    print("Calculating TLK")
    non_orphan_indices = np.array([order_per_protein[target_protein] for target_protein in other_proteins]).flatten()
    orphan_indices = orphan_indices
    K_X = gram_matrix
    #K_X = K_X[:,non_orphan_indices]


    K_T = None
    for t1 in proteins:
        K_t1 = None
        for t2 in proteins:
            if K_t1 is None:
                #minLines equals the amount of ligands per protein
                K_t1 = protein_sims[t1][t2] * np.ones((minLines,minLines))
            else:
                K_t1 = np.hstack([K_t1,protein_sims[t1][t2] * np.ones((minLines,minLines))])
        if K_T is None:
            K_T = K_t1
        else:
            K_T = np.vstack([K_T,K_t1])

    K_TL = np.multiply(K_X,K_T)

    y_s = np.array([protein_XY_dict[target]["y"] for target in proteins]).flatten()
    y_train = y_s[non_orphan_indices]

    # Train SVR with K_TL
    best_rmse = np.inf
    best_parameter = None

    best_kt = -1
    best_parameter_kt = None
    print("Determining best parameters for TLK")
    for parameter in param_tuples:
        p_rmse = []
        p_kt = []
        for train_index, test_index in kf.split(non_orphan_indices):
            relevant_gram = K_TL[non_orphan_indices,:]
            relevant_gram = relevant_gram[:,non_orphan_indices]
            relevant_gram = relevant_gram[train_index,:]
            relevant_gram = relevant_gram[:,train_index]
            Xids = non_orphan_indices
            Xids = Xids[train_index]
            svs, coefs = svr.svr_train(relevant_gram, Xids , y_train[train_index], parameter["C"], parameter["epsilon"])

            K_test_svs = K_TL[non_orphan_indices, :]
            K_test_svs = K_test_svs[test_index,:]
            K_test_svs= K_test_svs[:,svs]


            y_pred = np.array(svr.predict(K_test_svs,np.array(coefs).T)).reshape((len(test_index)))

            p_rmse.append(rmse(y_pred, y_train[test_index]))
            p_kt.append(kendalltau(y_pred,y_train[test_index])[0])

        average_rmse = np.average(p_rmse)
        if best_rmse > average_rmse:
            print("Current best RMSE for TLK: {}".format(average_rmse))
            #protein_models[protein] = (svs,coefs)
            best_rmse = average_rmse
            best_parameter = parameter

        average_kt = np.average(p_kt)
        if average_kt > best_kt:
            print("Current best KT for TLK: {}".format(average_kt))
            #protein_models[protein] = (svs,coefs)
            best_kt = average_kt
            best_parameter_kt = parameter

    relevant_gram = K_TL[non_orphan_indices, :]
    relevant_gram = relevant_gram[:, non_orphan_indices]
    Xids = np.array(non_orphan_indices)
    svs, coefs = svr.svr_train(relevant_gram, Xids, y_train, best_parameter["C"], best_parameter["epsilon"])

    K_test_svs = K_TL[orphan_indices, :]
    K_test_svs = K_test_svs[indices, :]
    K_test_svs = K_test_svs[:, svs]

    y_pred = np.array(svr.predict(K_test_svs, np.array(coefs).T)).reshape((len(indices)))
    y_orphan_test = y_s[orphan_indices]
    y_orphan_test = y_orphan_test[indices]
    TLK_rmse = rmse(y_pred, y_orphan_test)

    svs, coefs = svr.svr_train(relevant_gram, Xids, y_train, best_parameter_kt["C"], best_parameter_kt["epsilon"])

    K_test_svs = K_TL[orphan_indices, :]
    K_test_svs = K_test_svs[indices, :]
    K_test_svs = K_test_svs[:, svs]

    y_pred = np.array(svr.predict(K_test_svs, np.array(coefs).T)).reshape((len(indices)))
    y_orphan_test = y_s[orphan_indices]
    y_orphan_test = y_orphan_test[indices]
    TLK_kt = kendalltau(y_pred, y_orphan_test)[0]

    ### Target-Ligand Kernel - Closest 3

    # building K_X for TLK - Closest 3
    print("Calculating TLK for Closest 3 only")
    non_orphan_proteins = closest_3_proteins
    non_orphan_indices = np.array([order_per_protein[target_protein] for target_protein in non_orphan_proteins]).flatten()
    K_X = gram_matrix
    # K_X = K_X[:,non_orphan_indices]

    K_T = None
    for t1 in proteins:
        K_t1 = None
        for t2 in proteins:
            if K_t1 is None:
                # minLines equals the amount of ligands per protein
                K_t1 = protein_sims[t1][t2] * np.ones((minLines, minLines))
            else:
                K_t1 = np.hstack([K_t1, protein_sims[t1][t2] * np.ones((minLines, minLines))])
        if K_T is None:
            K_T = K_t1
        else:
            K_T = np.vstack([K_T, K_t1])

    K_TL = np.multiply(K_X, K_T)

    y_s = np.array([protein_XY_dict[target]["y"] for target in proteins]).flatten()
    y_train = y_s[non_orphan_indices]

    # Train SVR with K_TL
    best_rmse = np.inf
    best_parameter = None

    best_kt = -1
    best_parameter_kt = None
    print("Determining best parameters for TLK")
    for parameter in param_tuples:
        p_rmse = []
        p_kt = []
        for train_index, test_index in kf.split(non_orphan_indices):
            relevant_gram = K_TL[non_orphan_indices, :]
            relevant_gram = relevant_gram[:, non_orphan_indices]
            relevant_gram = relevant_gram[train_index, :]
            relevant_gram = relevant_gram[:, train_index]
            Xids = non_orphan_indices
            Xids = Xids[train_index]
            svs, coefs = svr.svr_train(relevant_gram, Xids, y_train[train_index], parameter["C"], parameter["epsilon"])

            K_test_svs = K_TL[non_orphan_indices, :]
            K_test_svs = K_test_svs[test_index, :]
            K_test_svs = K_test_svs[:, svs]

            y_pred = np.array(svr.predict(K_test_svs, np.array(coefs).T)).reshape((len(test_index)))

            p_rmse.append(rmse(y_pred, y_train[test_index]))
            p_kt.append(kendalltau(y_pred, y_train[test_index])[0])

        average_rmse = np.average(p_rmse)
        if best_rmse > average_rmse:
            print("Current best RMSE for TLK: {}".format(average_rmse))
            # protein_models[protein] = (svs,coefs)
            best_rmse = average_rmse
            best_parameter = parameter

        average_kt = np.average(p_kt)
        if average_kt > best_kt:
            print("Current best KT for TLK: {}".format(average_kt))
            # protein_models[protein] = (svs,coefs)
            best_kt = average_kt
            best_parameter_kt = parameter

    relevant_gram = K_TL[non_orphan_indices, :]
    relevant_gram = relevant_gram[:, non_orphan_indices]
    Xids = np.array(non_orphan_indices)
    svs, coefs = svr.svr_train(relevant_gram, Xids, y_train, best_parameter["C"], best_parameter["epsilon"])

    K_test_svs = K_TL[orphan_indices, :]
    K_test_svs = K_test_svs[indices, :]
    K_test_svs = K_test_svs[:, svs]

    y_pred = np.array(svr.predict(K_test_svs, np.array(coefs).T)).reshape((len(indices)))
    y_orphan_test = y_s[orphan_indices]
    y_orphan_test = y_orphan_test[indices]
    TLK_clo3_rmse = rmse(y_pred, y_orphan_test)

    svs, coefs = svr.svr_train(relevant_gram, Xids, y_train, best_parameter_kt["C"], best_parameter_kt["epsilon"])

    K_test_svs = K_TL[orphan_indices, :]
    K_test_svs = K_test_svs[indices, :]
    K_test_svs = K_test_svs[:, svs]

    y_pred = np.array(svr.predict(K_test_svs, np.array(coefs).T)).reshape((len(indices)))
    y_orphan_test = y_s[orphan_indices]
    y_orphan_test = y_orphan_test[indices]
    TLK_clo3_kt = kendalltau(y_pred, y_orphan_test)[0]


    print("Test Set Performance RMSE:")
    print("NLCP: {}".format(nlcp_rmse))
    print("Simplified: {}".format(simplified_rmse))
    print("TLK: {}".format(TLK_rmse))
    print("TLK-Clo-3: {}".format(TLK_clo3_rmse))
    print("Averaged: {}".format(average_rmse))
    print("Averaged-CLo-3: {}".format(averaged_3_rmse))
    print("Closest Protein {}: {}".format(closest_protein,closest_rmse))
    print("Farthest Protein {}: {}".format(farthest_protein,farthest_rmse))
    print("Average Y: {}".format(average_y_rmse))
    print("Best Protein {}: {}".format(best_protein,best_protein_rmse))
    print("Super-5: {}".format(super_05_rmse))
    print("Super-10: {}".format(super_10_rmse))
    print("Super-30: {}".format(super_30_rmse))
    print("Super-50: {}".format(super_50_rmse))
    print("Super-80: {}".format(super_80_rmse))

    print("Test Set Performance KT:")
    print("NLCP: {}".format(nlcp_kt))
    print("Simplified: {}".format(simplified_kt))
    print("TLK: {}".format(TLK_kt))
    print("TLK-Clo-3: {}".format(TLK_clo3_kt))
    print("Averaged: {}".format(average_kt))
    print("Averaged-CLo-3: {}".format(averaged_3_kt))
    print("Closest Protein {}: {}".format(closest_protein,closest_kt))
    print("Farthest Protein {}: {}".format(farthest_protein,farthest_kt))
    print("Average Y: {}".format(average_y_kt))
    print("Best Protein {}: {}".format(best_protein,best_protein_kt))
    print("Super-5: {}".format(super_05_kt))
    print("Super-10: {}".format(super_10_kt))
    print("Super-30: {}".format(super_30_kt))
    print("Super-50: {}".format(super_50_kt))
    print("Super-80: {}".format(super_80_kt))


    #nlcp_res_file.write("{};{};{};{};{}\n".format(protein,best_test,super_10))
    rmse_file.write("{};{};{};{};{};{};{};{};{};{};{};{};{};{};{};{}\n".format(orphan,nlcp_rmse,simplified_rmse,TLK_rmse,TLK_clo3_rmse,average_rmse,averaged_3_rmse,closest_rmse,farthest_rmse,average_y_rmse,best_protein_rmse,super_05_rmse,super_10_rmse,super_30_rmse,super_50_rmse,super_80_rmse))
    kt_file.write("{};{};{};{};{};{};{};{};{};{};{};{};{};{};{};{}\n".format(orphan,nlcp_kt,simplified_kt,TLK_kt,TLK_clo3_kt,average_kt,averaged_3_kt,closest_kt,farthest_kt,average_y_kt,best_protein_kt,super_05_kt, super_10_kt, super_30_kt, super_50_kt, super_80_kt))


rmse_file.close()
kt_file.close()

