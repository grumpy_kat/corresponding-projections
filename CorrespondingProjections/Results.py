import pickle
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
kernel = "linear"

result_dfs = []

per_protein_df = []


for draw in range(10):
    draw_folder = "../data/fixed_datasets/minLines_240_undersampling_True_numberDraws_10/" + str(draw)
    print("Draw {}".format(draw))
    #ädraw_folder = "../data/fixed_datasets/rmse_240_undersampling_True_numberDraws_10/"+str(draw)

    max_entry = pickle.load(open(draw_folder + "/max_entry.p", "rb"))
    minLines = pickle.load(open(draw_folder + "/minLines.p", "rb"))
    double_check = pickle.load(open(draw_folder + "/double_check.p", "rb"))
    maxProteins = pickle.load(open(draw_folder + "/maxProteins.p", "rb"))

    nlcp_res_file = pd.read_csv(draw_folder+"/rmse_"+str(minLines)+"_Ligands_"+str(maxProteins)+"_MaxProteins_Kernel_"+kernel+"_DoubleCheck_"+str(double_check),sep=";",index_col=0,header=0)
    nlcp_res = nlcp_res_file[["NLCP", "TLK", "TLK-3", "AVERAGED", "AVERAGED-3", "CLOSEST", "FARTHEST","SUPER5","SUPER10","SUPER30","SUPER50","SUPER80"]]

    per_protein_df.append(nlcp_res)
    result_dfs.append(nlcp_res.mean(axis=0))

per_protein_dict = {}

print(per_protein_df)

for protein in per_protein_df[0].index:
    per_protein_dict[protein] = {}
    for method in per_protein_df[0].columns.values:
        if method not in per_protein_dict:
            per_protein_dict[protein][method] = []
        for draw in range(10):
            per_protein_dict[protein][method].append(per_protein_df[draw].loc[protein][method])

for protein in per_protein_dict.keys():
    protein_dict = per_protein_dict[protein]
    protein_list = []
    for method in protein_dict.keys():
        protein_list.append(protein_dict[method])

    plt.figure().set_size_inches((5,5))

    plt.title(protein)
    plt.boxplot(np.array(protein_list).transpose())
    ax = plt.axes()
    bars = ["CP", "TLK", "TLK-Clo-3", "Avg", "Avg-Clo-3", "Closest Protein", "Farthest Protein","Supervised-5%","Supervised-10%","Supervised-30%","Supervised-50%","Supervised-80%"]
    plt.xticks(np.arange(len(bars))+1,bars,rotation=45,fontsize=15, ha="right")
    plt.yticks(fontsize=15)
    ax.yaxis.grid(True,alpha=0.3,which="both")
    plt.savefig("img/average_kt_protein"+protein+".pdf")

    plt.show()





res = np.array([r.as_matrix() for r in result_dfs])
#print(res.T)
#print([np.median(lis) for lis in res.T])


#bars = ["CP","TLK","TLK-Clo-3","Avg","Avg-Clo-3","Closest Protein","Farthest Protein"]
#bars = ["CP","Simplified"]
#bars = ["CP","Simplified"]
bars = ["CP", "TLK", "TLK-Clo-3", "Avg", "Avg-Clo-3", "Closest Protein", "Farthest Protein","Supervised-5%","Supervised-10%","Supervised-30%","Supervised-50%","Supervised-80%"]


plt.figure().set_size_inches((5,5))

plt.boxplot(res)
ax = plt.axes()
plt.xticks(np.arange(len(bars))+1,bars,rotation=45,fontsize=15, ha="right")
plt.yticks(fontsize=15)
ax.yaxis.grid(True,alpha=0.3,which="both")
plt.savefig("rmse_all_draws_all_proteins")
plt.show()